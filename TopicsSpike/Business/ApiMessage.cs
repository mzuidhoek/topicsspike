﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopicSpike.Shared.Business;
using TopicsSpike.AgentsImpl;

namespace TopicsSpike.Business
{
    public class ApiMessage : Message
    {
        private readonly TopicAgent _topicAgent = new TopicAgent();

        public bool PlaceInTopic()
        {
            return _topicAgent.Post(Entity);
        }
    }
}
