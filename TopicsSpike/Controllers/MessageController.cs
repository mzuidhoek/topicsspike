﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TopicSpike.Shared.Domain;
using TopicsSpike.Business;

namespace TopicsSpike.Controllers
{
    public class MessageController : ApiController
    {
        [HttpPost]
        public IHttpActionResult PostMessage(MessageEntity message)
        {
            ApiMessage messageHandler = new ApiMessage() { Entity = message };
            if(!messageHandler.IsValid())
            {
                return BadRequest();
            }
            if (messageHandler.PlaceInTopic())
            {
                return Ok();
            }
            else
            {
                return InternalServerError();
            }
        }
    }
}
