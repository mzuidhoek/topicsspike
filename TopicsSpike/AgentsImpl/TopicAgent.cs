﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopicSpike.Shared.Domain;
using TopicsSpike.Configuration;

namespace TopicsSpike.AgentsImpl
{
    public class TopicAgent
    {
        public TopicAgent()
        {
            SetupTopic();
        }

        private void SetupTopic()
        {
            var namespaceManager = NamespaceManager.CreateFromConnectionString(ApiSettings.ServiceBusConnectionString);

            if (!namespaceManager.TopicExists(ApiSettings.ServiceBusTopic))
            {
                namespaceManager.CreateTopic(ApiSettings.ServiceBusTopic);
            }
        }

        public bool Post(MessageEntity message)
        {
            string serializedMessage = JsonConvert.SerializeObject(message);
            BrokeredMessage topicMessage = new BrokeredMessage(serializedMessage);
            topicMessage.Properties["Priority"] = message.Priority;
            TopicClient client = TopicClient.CreateFromConnectionString(ApiSettings.ServiceBusConnectionString, ApiSettings.ServiceBusTopic);

            try
            {
                client.Send(topicMessage);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
