﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TopicSpike.Shared.Domain
{
    public class MessageEntity
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int Priority { get; set; }
    }
}
