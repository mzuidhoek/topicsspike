﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopicSpike.Shared.Domain;

namespace TopicSpike.Shared.Business
{
    public class Message
    {
        public MessageEntity Entity { get; set; }

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(Entity.Title);
        }
    }
}
