﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopicSpike.Shared.Domain;
using TopicsSpike.Receiver.Configuration;

namespace TopicSpike.Receiver.AgentsImpl
{
    public class SubscriptionAgent
    {
        private List<MessageEntity> _fetchedMessages = new List<MessageEntity>();
        private static SubscriptionAgent _agent;
        public static SubscriptionAgent Agent
        {
            get
            {
                if (_agent == null)
                {
                    _agent = new SubscriptionAgent();
                }
                return _agent;
            }
        }
        private SubscriptionAgent()
        {
            SetupSubscription();
            SetupListener();
        }

        private void SetupSubscription()
        {
            var namespaceManager = NamespaceManager.CreateFromConnectionString(ApiSettings.ServiceBusConnectionString);
            if (!namespaceManager.SubscriptionExists(ApiSettings.ServiceBusTopic, ApiSettings.ServiceBusSubscriptionName))
            {
                var filter = new SqlFilter(ApiSettings.SubscriptionFilter);
                namespaceManager.CreateSubscription(ApiSettings.ServiceBusTopic, ApiSettings.ServiceBusSubscriptionName, filter);
            }
        }

        private void SetupListener()
        {
            var client = SubscriptionClient.CreateFromConnectionString(ApiSettings.ServiceBusConnectionString, ApiSettings.ServiceBusTopic, ApiSettings.ServiceBusSubscriptionName);
            OnMessageOptions options = new OnMessageOptions()
            {
                AutoComplete = false,
                AutoRenewTimeout = TimeSpan.FromMinutes(1)
            };

            client.OnMessage((message) =>
            {
                try
                {
                    var entity = JsonConvert.DeserializeObject<MessageEntity>(message.GetBody<string>());
                    _fetchedMessages.Add(entity);
                    message.Complete();
                }
                catch
                {
                    message.Abandon();
                }
            }, options);
        }

        public List<MessageEntity> FetchNewMessages()
        {
            var listToReturn = new List<MessageEntity>(_fetchedMessages);
            _fetchedMessages.Clear();
            return listToReturn;
        }
    }
}
