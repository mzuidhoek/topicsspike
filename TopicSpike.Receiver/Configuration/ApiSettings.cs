﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TopicsSpike.Receiver.Configuration
{
    public class ApiSettings
    {

        public static string ServiceBusConnectionString { get { return GetStringValue(); } }
        public static string ServiceBusTopic { get { return GetStringValue(); } }
        public static string ServiceBusSubscriptionName { get { return GetStringValue(); } }
        public static string SubscriptionFilter { get { return GetStringValue(); } }

        private static string GetStringValue([CallerMemberName] string name = null)
        {
            return ConfigurationManager.AppSettings[name];
        }
    }
}
