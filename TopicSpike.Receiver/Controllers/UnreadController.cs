﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TopicSpike.Receiver.Business;

namespace TopicSpike.Receiver.Controllers
{
    public class UnreadController : ApiController
    {
        private ApiMessage _message = new ApiMessage();

        [HttpGet]
        public IHttpActionResult GetUnreadMessages()
        {
            var messages = _message.FetchMessages();

            return Ok(messages);
        }
    }
}
