﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopicSpike.Receiver.AgentsImpl;
using TopicSpike.Shared.Business;
using TopicSpike.Shared.Domain;

namespace TopicSpike.Receiver.Business
{
    public class ApiMessage : Message
    {
        private readonly SubscriptionAgent _agent = SubscriptionAgent.Agent;

        public List<MessageEntity> FetchMessages()
        {
            return _agent.FetchNewMessages();
        }
    }
}
